import React from 'react';
import { StyleSheet, View, ActivityIndicator, StatusBar, ImageBackground, Image } from 'react-native';
import AppNavigator from './navigation/AppNavigator';
import Footer from './components/Footer';
import NavigationService from './services/NavigationService';
import {Constants, Notifications, Permissions, Location, TaskManager, AppLoading, Asset} from 'expo';
import ResponseJson from './data.js';
import Tiles from './tiles.js';


function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheTiles(images) {
  return images.map(image => {
    return Image.prefetch('https://diamante.viewlab.io/tiles/namibia/' + image);
  });
}


export default class App extends React.Component {

  state = {
    isLoading: true,
    isReady: false,
    data: [],
    activeTab: {slug: 'map'},
    locations: []
  };

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          style={styles.container}
          startAsync={this._loadAssetsAsync.bind(this)}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }

    // if (this.state.isLoading) {
    //   return (
    //     <View style={styles.container}>
    //       <ActivityIndicator size="large"/>
    //     </View>
    //   );
    // }

    let app = this;

    return (
      <ImageBackground source={require('./assets/img_bk.jpg')} style={{width: '100%', height: '100%'}}>
        <View style={styles.container}>
          <View style={{height: 25, marginBottom: 1}} backgroundColor="rgba(0,0,0,.75)" barStyle="light-content" />
          <AppNavigator
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef, app);
            }}
          />
          <Footer data={this.state.data} activeRoute={this.state.activeRoute} />
        </View>
      </ImageBackground>
    );
  }

  getImages(responseJson, images) {
    let app = this;
    responseJson.map(function(item) {
      item.videos.map(function(video) {
        images.push(video.poster);
        images.push(video.file);
      });
      images = app.getImages(item.children, images);
    });

    return images;
  }

  async _loadAssetsAsync() {
    let responseJson = ResponseJson;
    NavigationService.setData(responseJson);
    this.setState({
      isLoading: false,
      data: responseJson,
    }, function(){

    });

    let images = [];
    images = this.getImages(responseJson, images);

    const imageAssets = cacheImages(images);
    const tilesAssets = cacheTiles(Tiles);

    // const imageAssets = cacheImages([]);
    // const tilesAssets = cacheTiles([]);

    await Promise.all([...imageAssets, ...tilesAssets]);
  }

  componentDidMount() {
    // let responseJson = require('./assets/data/data.json');
    // let responseJson = ResponseJson;
    // NavigationService.setData(responseJson);

    // this.setState({
    //   isLoading: false,
    //   data: responseJson,
    // }, function(){
      this.setupNotifications().then(function() {});
      // this._getLocationAsync();
    // });
  }

  setupNotifications = async () => {
    let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

    if (Constants.isDevice && result.status === 'granted') {
      console.log('Notification permissions granted.')
    } else {
      alert('You need enable permissions in settings.');
      return false;
    }

    Notifications.addListener(this.handleNotification.bind(this));


    // let localNotification = {
    //   title: 'title',
    //   body: 'body here',
    //   data: ['itinerari', 'storico', 'gran-madre-di-dio', 'Gran Madre.WebM']
    // };
    // Notifications.presentLocalNotificationAsync(localNotification);

    return true;
  };

  handleNotification(notification) {
    if (notification.origin == 'selected') {
      setTimeout(function() {
        let [tabSlug, listSlug, subListSlug, videoSlug] = notification.data;
        NavigationService.go(tabSlug, listSlug, subListSlug, videoSlug);
      }, 1000);
    }
  }

  setupLocation() {

  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      alert('Permission to access location was denied.');
      return false;
    }

    // let location = await Location.getCurrentPositionAsync({});
    // console.log('location', location);

    let app = this;
    TaskManager.defineTask('locationUpdates', ({ data: { locations }, error }) => {
      if (error) {
        console.warn('Location updates error:', error);
        // check `error.message` for more details.
        return;
      }
      console.warn('Received new locations:', locations);
      app.setState({
        locations: locations
      })
    });
    Location.startLocationUpdatesAsync('locationUpdates', {});

    return true;
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
