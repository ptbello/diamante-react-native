import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import NavigationService from '../services/NavigationService';

export default class Footer extends React.Component {

  state = {
    active: 'map'
  };

  render() {
    let [tabSlug] = NavigationService.getRouteParams();
    const items = this.props.data.map((tab, index) => this.renderItem(tab, tabSlug));

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{flex:1}}
          key="map"
          onPress={() => this.navigate('Map', {slug: 'map'})}
        >
          <Text style={'map' == tabSlug ? styles.activeTab : styles.inActiveTab}>
            Map
          </Text>
        </TouchableOpacity>
        {items}
      </View>
    );
  }

  renderItem = (tab, tabSlug) => {
    return (
      <TouchableOpacity
        style={{flex:1}}
        key={tab.slug}
        onPress={() => this.navigate('Page', {slug: tab.slug, tab: tab })}
        activeOpacity={0.8}
      >
        <Text style={tab.slug == tabSlug ? styles.activeTab : styles.inActiveTab}>
          {tab.title}
        </Text>
      </TouchableOpacity>
    )
  }

  navigate = (route, params) => {
    this.setState({
      active: params.slug
    });
    NavigationService.go(params.slug);
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    height: 40,
    alignContent: 'center',
    marginTop: 1,
  },
  activeTab: {
    color: '#f2c335',
    textAlign: 'center',
    backgroundColor: '#000',
    height: 40,
  },
  inActiveTab: {
    color: '#f2c335',
    backgroundColor: 'rgba(0,0,0,.75)',
    textAlign: 'center',
    height: 40,
  }
});
