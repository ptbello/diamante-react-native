import { NavigationActions } from 'react-navigation';

let _navigator;
let _app;
let _currentParams = {};
let _routeParams = ['map'];

let _activeTab = {slug: 'map'};

let _data = {};

function setTopLevelNavigator(navigatorRef, app) {
  _navigator = navigatorRef;
  _app = app;
}

function getApp() {
  return _app;
}

function navigate(routeName, params) {
  _activeTab = params;

  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );

  _app.setState({
    activeTab: {slug: params.slug}
  })
}

function getActiveTab() {
  return _activeTab;
}

function setData(data) {
  _data = data;
}

function getData() {
  return _data;
}

function go(tabSlug, listSlug, subListSlug, videoSlug) {
  _routeParams = [tabSlug, listSlug, subListSlug, videoSlug];
  console.log('go>>>>>>>', tabSlug, listSlug, subListSlug, videoSlug);

  if (tabSlug == 'map') {
    navigate('Map', {slug: 'map'})
    return;
  }

  let tab;
  for (let k = 0; k <= _data.length; k++) {
    if (_data[k].slug == tabSlug) {
      tab = _data[k];
      break;
    }
  }

  let list;
  if (listSlug) {
    for (let i = 0; i < tab.children.length; i++) {
      if (tab.children[i].slug == listSlug) {
        list = tab.children[i];
        break;
      }
    }
  }

  let subList;
  if (subListSlug) {
    for (let j = 0; j < list.children.length; j++) {
      if (list.children[j].slug == subListSlug) {
        subList = list.children[j];
        break;
      }
    }
  }

  let videos = [];
  if (!list && !subList) {
    for (let i = 0; i < tab.children.length; i++) {
      let _list = tab.children[i];

      let tmp = []
      _list.videos.map(function(item) {
        item.url = [tabSlug, _list.slug, null, item.title];
        tmp.push(item);
      });
      videos = videos.concat(tmp);

      for (let j = 0; j < _list.children.length; j++) {
        let tmp = [];
        tab.children[i].children[j].videos.map(function(item) {
          item.url = [tabSlug, _list.slug, _list.children[j].slug, item.title];
          tmp.push(item);
        });
        videos = videos.concat(tmp);
      }
    }
  }

  if (subList) {
    let tmp = [];
    subList.videos.map(function(item) {
      item.url = [tabSlug, list.slug, subList.slug, item.title];
      tmp.push(item);
    });
    videos = videos.concat(tmp);
  } else if (list) {
    let tmp = [];
    list.videos.map(function(item) {
      item.url = [tabSlug, list.slug, null, item.title];
      tmp.push(item);
    });
    videos = videos.concat(tmp);

    for (let i = 0; i < list.children.length; i++) {
      let tmp = [];
      list.children[i].videos.map(function(item) {
        item.url = [tabSlug, list.slug, list.children[i].slug, item.title];
        tmp.push(item);
      });
      videos = videos.concat(tmp);
    }
  }

  videos = videos.filter(v => v.file);

  let video;
  if (videoSlug) {
    for (let y = 0; y < videos.length; y++) {
      if (videos[y].title = videoSlug) {
        video = videos[y];
        break;
      }
    }
  }

  _currentParams = {
    tab: tab,
    active: subListSlug || listSlug,
    video: video,
    videos: videos,
  };

  navigate('Page', _currentParams);

  _app.setState({
    activeTab: tabSlug
  });
}

function getParams() {
  return _currentParams;
}

function getParam(key, def) {
  return _currentParams[key] || def;
}

function getRouteParams() {
  return _routeParams;
}

function getLocations() {
  return _app ? _app.state.locations : [];
}


import { Dimensions } from 'react-native';
const { height, width } = Dimensions.get( 'window' );
const LATITUDE_DELTA = 5;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);

let _region = {
  latitude: -22.7357,
  longitude: 17.0947,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};
function getRegion() {
  return _region;
}
function setRegion(region) {
  _region = region;
}

// add other navigation functions that you need and export them

export default {
  getActiveTab,
  navigate,
  setTopLevelNavigator,
  getApp,
  setData,
  getData,
  go,
  getParam,
  getParams,
  getRouteParams,
  getLocations,
  getRegion,
  setRegion,
};
