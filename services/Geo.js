// import * as assets from './assets';
import {Notifications} from 'expo';
import {Vibration} from 'react-native';

/**
 Geo location
 */
var markers = []
export function geoSetup(mrks, position) {
    markers = mrks
    geoCheckThreshold(position)
}

function geoDeg2Rad(deg) {
    return deg * Math.PI / 180;
}
function geoDistance( pos1, pos2 ) {
    var lat1, lon1, lat2, lon2;
    lat1 = pos1[0];
    lon1 = pos1[1];
    lat2 = pos2[0];
    lon2 = pos2[1];
    // Pythagoras Equirectangular(lat1, lon1, lat2, lon2)
    lat1 = geoDeg2Rad(lat1);
    lat2 = geoDeg2Rad(lat2);
    lon1 = geoDeg2Rad(lon1);
    lon2 = geoDeg2Rad(lon2);
    var R = 6371; // km
    var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
    var y = (lat2 - lat1);
    var d = Math.sqrt(x * x + y * y) * R;

    return d;
}
function geoCheckThreshold(position) {
    var i, distance;
    var yourhere = [position.coords.latitude, position.coords.longitude];

    for (i = 0; i < markers.length; i++) {
        distance = geoDistance(yourhere, markers[i].position);
        if (distance < parseFloat(markers[i].ron)) {
            if (!markers[i].isNotified) {
                markers[i].isNotified = true;
                geoNotify(markers[i]);
            }
        } else if (distance > parseFloat(markers[i].roff)) {
            markers[i].isNotified = false;
        }
    }
}

function geoNotify(marker) {
  Vibration.vibrate(500);

  let [empty, tabSlug, listSlug, subListSlug] =  marker.url.split('/');
  let localNotification = {
    // title: 'Il Meteorite di Hoba' + " in avvicinamento!",
    title: marker.title + ' in avvicinamento!',
    body: 'guarda i video',
    // data: ['itinerari', 'nord-namibia', 'il-meteorite-di-hoba', 'Il Meteorite di Hoba']
    data: [tabSlug, listSlug, subListSlug, marker.title]
  };
  console.log('localNotification.data', localNotification.data);
  Notifications.presentLocalNotificationAsync(localNotification);

  return;

    var notifyTitle = marker.title + " in avvicinamento!";
    var notifyBody = "guarda i video";
    var notifyImage = "/imgs/ico-4.png";
    var options = {
        "body": notifyBody,
        "icon": notifyImage,
        "data": assets.absolute('#' + marker.url),
        "requireInteraction": true,
        "vibrate": [300, 100, 400]
    };

    console.log( 'Notification!', notifyTitle, options);
    navigator.serviceWorker.ready.then(function(registration) {
        registration.showNotification( notifyTitle, options);
    });
}
