import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

// import TabBarIcon from '../components/TabBarIcon';
import MapScreen from '../screens/MapScreen';
import PageScreen from '../screens/PageScreen';

const MapStack = createStackNavigator({
  Map: MapScreen,
});
MapStack.navigationOptions = {
  tabBarLabel: 'Map',
};

const PageStack = createStackNavigator({
  Page: PageScreen,
});
PageStack.navigationOptions = {
  tabBarLabel: 'Page',
};

export default createBottomTabNavigator({
  MapStack,
  PageStack,
});
