import React from 'react';
import { StyleSheet, Text, View, Button, Platform } from 'react-native';
import NavigationService from '../services/NavigationService';
import {Constants, Notifications, Permissions, Location, TaskManager, MapView} from 'expo';
import { Marker, Circle, UrlTile } from 'react-native-maps';
import * as geo from './../services/Geo';
import {Vibration} from 'react-native';

let isInicialized = false;

TaskManager.defineTask('locationUpdates1', ({ data: { locations }, error }) => {
  if (error) {
    console.warn('Location updates error:', error);
    // check `error.message` for more details.
    return;
  }

  // console.log('Received new locations:', locations);
  // Vibration.vibrate(500);
  // Notifications.presentLocalNotificationAsync({
  //   title: 'Received new locations',
  //   body: JSON.stringify(locations),
  //   data: ['itinerari', 'nord-namibia', 'il-meteorite-di-hoba', 'Il Meteorite di Hoba'],
  // });

  // let app = NavigationService.getApp();
  // app.setState({
  //   locations: locations
  // });
  //
  // if (!isInicialized) {
  //   setInterval(function () {
  //     Vibration.vibrate(500);
  //     Notifications.presentLocalNotificationAsync({
  //       title: 'Every 30 seconds',
  //       body: 'Every 30 seconds',
  //       data: ['itinerari', 'nord-namibia', 'il-meteorite-di-hoba', 'Il Meteorite di Hoba'],
  //     });
  //   }, 30000);
  //   isInicialized = true;
  //
  //   Vibration.vibrate(500);
  //   Notifications.presentLocalNotificationAsync({
  //     title: 'Initialized: setInterval',
  //     body: 'Initialized: setInterval',
  //     data: ['itinerari', 'nord-namibia', 'il-meteorite-di-hoba', 'Il Meteorite di Hoba'],
  //   });
  // }

  // setTimeout(function() {
    var data = NavigationService.getData();
    let markers = getMarkers(data);
    // app.mapShowCurrentPosition(position.coords.latitude, position.coords.longitude);
    geo.geoSetup(markers, locations[0]);
  // }, 9000);
});

function getMarkers(data) {
  var markers = [];
  var ron_default = 10;
  var roff_default = 40;
  var ratio_default = 2;

  function mapMarkersBuild( nodes, parent ) {
    for( var i=0; i<nodes.length; i++ ) {
      var node = nodes[i];
      var children = node.children;
      if( node.lat && node.lng ) {
        var marker = {
          "title": node.title,
          "slug": node.slug,
          "url": parent + '/' + node.slug,
          "position": [node.lat, node.lng],
          latlng: {
            latitude: parseFloat(node.lat),
            longitude: parseFloat(node.lng),
          },
          "ron": node.ron ? node.ron : ron_default,
          "roff": node.roff ? node.roff : roff_default,
          "videos": node.videos,
          "directions": "https://maps.google.com/maps?daddr=" + node.lat + "," + node.lng
        };
        if(marker.roff <= marker.ron) {
          marker.roff = marker.ron * ratio_default;
        }
        markers.push(marker);
      }

      if(Array.isArray(children) && children.length ) {
        mapMarkersBuild( children, parent + '/' + node.slug );
      }
    }
  }
  mapMarkersBuild(data, '')

  return markers;
}

export default class MapScreen extends React.Component {
  state = {
    markers: [],
    locations: [],
  };

  render() {
    var data = NavigationService.getData();
    let markers = getMarkers(data);
    let region = NavigationService.getRegion();

    return (
      <MapView
        style={{ flex: 1 }}
        minZoomLevel={6}
        maxZoomLevel={12}
        region={region}
        onRegionChangeComplete={this.onRegionChange.bind(this)}
        showsUserLocation={true}
        showsMyLocationButton={true}
        mapType={Platform.OS == "android" ? "none" : "standard"}
      >
        <UrlTile
          /**
          * The url template of the tile server. The patterns {x} {y} {z} will be replaced at runtime
          * For example, http://c.tile.openstreetmap.org/{z}/{x}/{y}.png
          */
          urlTemplate={'https://diamante.viewlab.io/tiles/namibia/{z}/{x}/{y}.png'}
          /**
           * The maximum zoom level for this tile overlay. Corresponds to the maximumZ setting in
           * MKTileOverlay. iOS only.
           */
          maximumZ={19}
        />
        {markers.map(marker => (
          <Marker
            key={marker.url}
            coordinate={marker.latlng}
            title={marker.title}
            description={'Mostra video'}
            onCalloutPress={event => {this.onCalloutPress(marker)}}
          />
        ))}
        {markers.map(marker => (
          <Circle
            key={'circleon' + marker.url}
            center={marker.latlng}
            radius={parseFloat(marker.ron)*1000}
          />
        ))}
        {markers.map(marker => (
          <Circle
            key={'circleoff' + marker.url}
            center={marker.latlng}
            radius={parseFloat(marker.roff)*1000}
          />
        ))}
      </MapView>
    );
  }

  onRegionChange(region) {
    NavigationService.setRegion(region);
  }

  onCalloutPress(marker) {
    let [empty, tabSlug, listSlug, subListSlug] =  marker.url.split('/');
    NavigationService.go(tabSlug, listSlug, subListSlug, marker.title);
  }

  showTime(timestamp) {
    // var date = new Date(timestamp * 1000);
    var date = new Date();

    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();

    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }

  componentDidMount() {
    this._getLocationAsync();
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      alert('Permission to access location was denied.');
      return false;
    }

    // let location = await Location.getCurrentPositionAsync({});
    // console.log('location', location);

    Location.startLocationUpdatesAsync('locationUpdates1', {
      accuracy: Location.Accuracy.High
    });

    return true;
  };

  fireNotification() {
    let localNotification = {
      title: 'Il Meteorite di Hoba' + " in avvicinamento!",
      body: 'guarda i video',
      data: ['itinerari', 'nord-namibia', 'il-meteorite-di-hoba', 'Il Meteorite di Hoba']
    };
    Notifications.presentLocalNotificationAsync(localNotification);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
