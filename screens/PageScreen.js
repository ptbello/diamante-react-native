import React from 'react';
import { StyleSheet, Text, View, ScrollView, Button, Image, TouchableOpacity, Modal, TouchableHighlight, Dimensions } from 'react-native';
import { Video } from 'expo';
import NavigationService from '../services/NavigationService';

export default class PageScreen extends React.Component {

  state = {
    active: '',
    modal: '',
  };

  render() {
    const { navigation } = this.props;
    const tab = navigation.getParam('tab', null);
    const videos = navigation.getParam('videos', null);
    this.state.active = navigation.getParam('active', '');

    const video = navigation.getParam('video', null);
    if (video) {
      this.state.modal = video;
    } else {
      this.state.modal = '';
    }

    let { width, height } = Dimensions.get('window');
    height = height - 100;

    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View key='tabs' style={{padding: 10, flex: 1}}>
          {tab.children.map((item, key) => {
            let parentItem = item;
            return (
              <View key={item.slug +'-'+ key} style={{color: '#f2c335', margin: 3}}>
                <Button

                  key={item.slug}
                  title={item.title}
                  onPress={() => this.onPress(item.slug)}
                  color={item.slug == this.state.active ? "#000" : "rgba(0,0,0,.55)"}
                />
                {
                  item.children.map((item, key) => {
                    return (
                      <View key={item.slug + '--' + key} style={{marginLeft: 40, marginTop: 3}}>
                        <Button
                          key={item.slug}
                          title={item.title}
                          onPress={() => this.onPress(parentItem.slug, item.slug)}
                          color={item.slug == this.state.active ? "#000" : "rgba(0,0,0,.55)"}
                        />
                      </View>
                    )
                  })
                }
              </View>
            )
          })}
        </View>

        <View key='videos' style={{padding: 10, flex: 1.5}}>
          {videos.map((video, key) => (
            <TouchableOpacity
              key={video.title + '-' + key}
              style={{marginBottom: 20, padding: 10, backgroundColor: 'rgba(0,0,0,.75)'}}
              onPress={()=> this.onVideoPress(video)}
            >
              <Image
                style={{width: 353, height: 200}}
                // source={{uri: 'https://diamante.viewlab.io/data/poster/' + video.poster}}
                source={video.poster}
              />
              <Text style={{color: '#f2c335'}}>{video.title}</Text>
            </TouchableOpacity>
          ))}
        </View>


        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modal ? true : false}
          onRequestClose={() => {}}
        >
          <View>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <TouchableHighlight
                style={{width: '100%'}}
                onPress={() => this.closeModal()}>
                <Text style={{backgroundColor: 'rgba(0,0,0,.75)', padding: 10}}>Close</Text>
              </TouchableHighlight>

              <Text>{this.state.modal.title}</Text>
              <Video
                // source={{uri: 'https://diamante.viewlab.io/data/video/' + (this.state.modal ? this.state.modal.file.replace('.WebM', '.mp4') : '') }}
                source={this.state.modal.file}
                shouldPlay
                useNativeControls
                resizeMode="cover"
                // style={{ width, height }}
                style={{ width: 480, height: 270 }}
                onLoad={()=> console.log('video loaded')}
                onError={(error)=> console.log('error', error)}
              />
            </View>
          </View>
        </Modal>
      </ScrollView>
    );
  }
// { uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' }
  

  onPress(listSlug, subListSlug) {
    const tab = NavigationService.getParam('tab', null);
    NavigationService.go(tab.slug, listSlug, subListSlug);
    // this.setState({
    //   modal: ''
    // });
  }

  _lastRoute = null;

  onVideoPress(video) {
    this._lastRoute = NavigationService.getRouteParams();

    let [tabSlug, listSlug, subListSlug, file] = video.url;
    NavigationService.go(tabSlug, listSlug, subListSlug, file);

    this.setState({
      modal: video
    });
  }

  closeModal() {
    this.state.modal = '';
    let [tabSlug, listSlug, subListSlug] = this._lastRoute ? this._lastRoute : NavigationService.getRouteParams();

    NavigationService.go(tabSlug, listSlug, subListSlug);
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'flex-start',
  },
});
