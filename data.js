export default [
  {
    "slug": "itinerari",
    "title": "Itinerari",
    "subtitle": "",
    "cover": "",
    "icon": "itinerari.png",
    "lat": "",
    "lng": "",
    "videos": [],
    "children": [
      {
        "slug": "nord-namibia",
        "title": "Nord Namibia",
        "subtitle": "highlights del nord",
        "cover": "nord.png",
        "icon": "",
        "lat": "",
        "lng": "",
        "videos": [],
        "children": [
          {
            "slug": "il-meteorite-di-hoba",
            "title": "Il Meteorite di Hoba",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-19.592565",
            "lng": "17.933702",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Il Meteorite di Hoba",
                "poster": require("./assets/data/poster/meteorite-hoba.png"),
                "file": require("./assets/data/video/meteorite-hoba.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "parco-nazionale-etosha-okauku",
            "title": "Parco Nazionale Etosha Okauku gate",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-19.330751",
            "lng": "15.94027",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Parco Etosha",
                "poster": require("./assets/data/poster/parco-etosha.png"),
                "file": require("./assets/data/video/parco-etosha.mp4")
              },
              {
                "title": "Florae Fauna Parco Etosha",
                "poster": require("./assets/data/poster/flora-fauna-parcoetosha.png"),
                "file": require("./assets/data/video/flora-fauna-parcoetosha.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "parco-nazionale-etosha-galton",
            "title": "Parco Nazionale Etosha Galton gate",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-19.314737",
            "lng": "14.481626",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Parco Etosha",
                "poster": require("./assets/data/poster/parco-etosha.png"),
                "file": require("./assets/data/video/parco-etosha.mp4")
              },
              {
                "title": "Florae Fauna Parco Etosha",
                "poster": require("./assets/data/poster/flora-fauna-parcoetosha.png"),
                "file": require("./assets/data/video/flora-fauna-parcoetosha.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "parco-nazionale-etosha-vonlindequist",
            "title": "Parco Nazionale Etosha Von Lindequist gate",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-18.803416",
            "lng": "17.04327",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Parco Etosha",
                "poster": require("./assets/data/poster/parco-etosha.png"),
                "file": require("./assets/data/video/parco-etosha.mp4")
              },
              {
                "title": "Florae Fauna Parco Etosha",
                "poster": require("./assets/data/poster/flora-fauna-parcoetosha.png"),
                "file": require("./assets/data/video/flora-fauna-parcoetosha.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "costa-degli-scheletri-nord",
            "title": "Costa degli Scheletri North gate",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.310656",
            "lng": "13.647682",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Costa degli Scheletri North gate",
                "poster": require("./assets/data/poster/costa-degli-scheletri.png"),
                "file": require("./assets/data/video/costa-degli-scheletri.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "costa-degli-scheletri-sud",
            "title": "Costa degli Scheletri South gate",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.442547",
            "lng": "13.818713",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Costa degli Scheletri South gate",
                "poster": require("./assets/data/poster/costa-degli-scheletri.png"),
                "file": require("./assets/data/video/costa-degli-scheletri.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "parco-nazionale-dorob",
            "title": "Parco Nazionale Dorob",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-22.407977",
            "lng": "14.449171",
            "ron": "50",
            "roff": "50.5",
            "videos": [
              {
                "title": "Parco Dorob",
                "poster": require("./assets/data/poster/parco-dorob.png"),
                "file": require("./assets/data/video/parco-dorob.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "la-grande-costa-della-namibia",
            "title": "La Grande Costa Delle Namibia",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "",
            "lng": "",
            "videos": [
              {
                "title": "La Grande Costa della Namibia",
                "poster": require("./assets/data/poster/GCDN.png"),
                "file": require("./assets/data/video/GCDN.mp4")
              },
              {
                "title": "Dati Ecologici Della Grande Costa della Namibia",
                "poster": require("./assets/data/poster/dati-eco-GCDN.png"),
                "file": require("./assets/data/video/dati-eco-GCDN.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "il-damaraland",
            "title": "Il Damaraland",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "",
            "lng": "",
            "videos": [
              {
                "title": "Il Damaraland",
                "poster": require("./assets/data/poster/damaraland.png"),
                "file": require("./assets/data/video/damaraland.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "la-montagna-bruciata",
            "title": "La Montagna Bruciata",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.620997",
            "lng": "14.418267",
            "ron": "2",
            "roff": "2,1",
            "videos": [
              {
                "title": "La Montagna Bruciata",
                "poster": require("./assets/data/poster/montagna-bruciata.png"),
                "file": require("./assets/data/video/montagna-bruciata.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "la-signora-bianca",
            "title": "La Signora Bianca",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.018307",
            "lng": "14.681545",
            "ron": "10",
            "roff": "10.1",
            "videos": [
              {
                "title": "La Signora Bianca ",
                "poster": require("./assets/data/poster/la-signora-bianca.png"),
                "file": require("./assets/data/video/la-signora-bianca.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "spitzkoppe",
            "title": "Spitzkoppe",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.824706",
            "lng": "15.16883",
            "ron": "12",
            "roff": "12.5",
            "videos": [
              {
                "title": "Spitzkoppe",
                "poster": require("./assets/data/poster/spitzkoppe.png"),
                "file": require("./assets/data/video/spitzkoppe.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "twyfelfontein",
            "title": "Twyfelfontein",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.592309",
            "lng": "14.372028",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "Twyfelfontein",
                "poster": require("./assets/data/poster/twyfelfontein.png"),
                "file": require("./assets/data/video/twifelfontein.mp4")
              },
              {
                "title": "Appunti sull’Arte Rupestre",
                "poster": require("./assets/data/poster/appunti-arte-rupestre.png"),
                "file": require("./assets/data/video/appunti-arte-rupestre.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "la-foresta-pietrificata",
            "title": "La Foresta Pietrificata",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.439137",
            "lng": "14.606102",
            "ron": "3",
            "roff": "3.1",
            "videos": [
              {
                "title": "La Foresta Pietrificata",
                "poster": require("./assets/data/poster/foresta-pietrificata.png"),
                "file": require("./assets/data/video/foresta-pietrificata.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "la-valle-delle-canne-d-organo",
            "title": "La Valle Delle Canne d Organo",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.612957",
            "lng": "14.415778",
            "ron": "2",
            "roff": "2.5",
            "videos": [
              {
                "title": "La Valle delle Canne D’Organo",
                "poster": require("./assets/data/poster/valle-canne-dorgano.png"),
                "file": require("./assets/data/video/valle-canne-dorgano.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "fingerklip",
            "title": "Fingerklip",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.382988",
            "lng": "15.434097",
            "ron": "3",
            "roff": "3.5",
            "videos": [
              {
                "title": "Fingerklip",
                "poster": require("./assets/data/poster/fingerklip.png"),
                "file": require("./assets/data/video/fingerklip.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "passo-grootberg",
            "title": "Passo Grootberg",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-19.84052",
            "lng": "14.125254",
            "ron": "2",
            "roff": "2.1",
            "videos": [
              {
                "title": "Il Passo Grootberg",
                "poster": require("./assets/data/poster/grootberg.png"),
                "file": require("./assets/data/video/grootberg.mp4")
              }
            ],
            "children": []
          }
        ]
      },
      {
        "slug": "centro",
        "title": "Centro Namibia",
        "subtitle": "highlights del centro",
        "cover": "centro.png",
        "icon": "",
        "lat": "",
        "lng": "",
        "videos": [],
        "children": [
          {
            "slug": "waterberg-plateau-park",
            "title": "Il Waterberg Plateau Park ",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-20.504594",
            "lng": "17.241229",
            "ron": "10",
            "roff": "11",
            "videos": [
              {
                "title": "Il Waterberg Plateau National Park",
                "poster": require("./assets/data/poster/waterberg-plateau-np.png"),
                "file": require("./assets/data/video/waterberg-plateau-np.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "montagne-erongo",
            "title": "Montagne Erongo",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.600703",
            "lng": "15.698703",
            "ron": "50",
            "roff": "51",
            "videos": [
              {
                "title": "Montagne Erongo",
                "poster": require("./assets/data/poster/montagne-erongo.png"),
                "file": require("./assets/data/video/montagne-erongo.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "phillips-cave",
            "title": "Phillip’s Cave",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.794605",
            "lng": "15.639715",
            "ron": "5",
            "roff": "6",
            "videos": [
              {
                "title": "Phillip’s Cave",
                "poster": require("./assets/data/poster/phillips-cave.png"),
                "file": require("./assets/data/video/phillips-cave.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "spitzkoppe",
            "title": "Spitzkoppe",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.824706",
            "lng": "15.16883",
            "ron": "3",
            "roff": "4",
            "videos": [
              {
                "title": "Spitzkoppe1",
                "poster": require("./assets/data/poster/spitzkoppe1.png"),
                "file": require("./assets/data/video/spitzkoppe1.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "brandberg",
            "title": "Brandberg",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.102687",
            "lng": "14.581616",
            "ron": "50",
            "roff": "51",
            "videos": [
              {
                "title": "Brandberg",
                "poster": require("./assets/data/poster/brandberg.png"),
                "file": require("./assets/data/video/brandberg.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "daan-viljoen-gp",
            "title": "Dana Viljoen Game Park",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-22.528256",
            "lng": "16.958161",
            "ron": "3",
            "roff": "4",
            "videos": [
              {
                "title": "Daan Viljoen Game Park",
                "poster": require("./assets/data/poster/daan-viljoen-gp.png"),
                "file": require("./assets/data/video/daan-viljoen-gp.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "montagne-omatakpo",
            "title": "Montagne Omatako",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.230081",
            "lng": "16.742093",
            "ron": "20",
            "roff": "21",
            "videos": [
              {
                "title": "Montagne Omatako",
                "poster": require("./assets/data/poster/montagne-omatako.png"),
                "file": require("./assets/data/video/montagne-omatako.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "terra-dei-dinosauri",
            "title": "La Terra dei Dinosauri",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.040569",
            "lng": "16.400516",
            "ron": "3",
            "roff": "4",
            "videos": [
              {
                "title": "La Terra dei Dinosauri",
                "poster": require("./assets/data/poster/terra-dei-dinosauri.png"),
                "file": require("./assets/data/video/terra-dei-dinosauri.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "regioni-attorno-swakopmund-walvis-bay",
            "title": "Le Regioni attorno Swakopmund e Wallis Bay",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-22.8183927",
            "lng": "14.6396684",
            "ron": "3",
            "roff": "4",
            "videos": [
              {
                "title": "Le Regioni attorno Swakopmund e Wallis Bay",
                "poster": require("./assets/data/poster/regioni-attorno-swakopmund-walvis-bay.png"),
                "file": require("./assets/data/video/regioni-attorno-swakopmund-walvis-bay.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "campi-licheni",
            "title": "Campi di licheni",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "",
            "lng": "",
            "ron": "",
            "roff": "",
            "videos": [
              {
                "title": "Campi di Licheni",
                "poster": require("./assets/data/poster/campi-licheni.png"),
                "file": require("./assets/data/video/campi-licheni.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "sandwich-harbour",
            "title": "Sandwich Harbour",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-23.390916",
            "lng": "14.474668",
            "ron": "5",
            "roff": "6",
            "videos": [
              {
                "title": "Sandwich Harbour",
                "poster": require("./assets/data/poster/sandwich-harbour.png"),
                "file": require("./assets/data/video/sandwich-harbour.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "cape-cross-seal-reserve",
            "title": "Cape Cross Seal Reserve",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-21.764035",
            "lng": "13.96233",
            "ron": "6",
            "roff": "7",
            "videos": [
              {
                "title": "Cape Cross Seal Reserve",
                "poster": require("./assets/data/poster/cape-cross-seal-reserve.png"),
                "file": require("./assets/data/video/cape-cross-seal-reserve.mp4")
              }
            ],
            "children": []
          }
        ]
      },
      {
        "slug": "sud",
        "title": "Sud Namibia",
        "subtitle": "highlights del sud",
        "cover": "sud.png",
        "icon": "",
        "lat": "",
        "lng": "",
        "videos": [],
        "children": [
          {
            "slug": "deserto-del-namib",
            "title": "Il Deserto del Namib ",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-24.48713",
            "lng": "15.798492",
            "ron": "5",
            "roff": "6",
            "videos": [
              {
                "title": "Intro Deserto del Namib",
                "poster": require("./assets/data/poster/intro-deserto-namib.png"),
                "file": require("./assets/data/video/intro-deserto-namib.mp4")
              },
              {
                "title": "Il Deserto Del Namib",
                "poster": require("./assets/data/poster/deserto-del-namib.png"),
                "file": require("./assets/data/video/deserto-del-namib.mp4")
              },
              {
                "title": "Flora e Fauna Deserto Del Namib",
                "poster": require("./assets/data/poster/flora-fauna-deserto-namib.png"),
                "file": require("./assets/data/video/flora-fauna-deserto-namib.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "sesriem-canyon",
            "title": "Sesriem Canyon",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-24.518366",
            "lng": "15.800164",
            "ron": "3",
            "roff": "4",
            "videos": [
              {
                "title": "Sesriem Canyo",
                "poster": require("./assets/data/poster/sesriem-canyon.png"),
                "file": require("./assets/data/video/sesriem-canyon.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "deserts-kalahari",
            "title": "Deserto del Kalahari",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-24.138852",
            "lng": "19.114855",
            "ron": "100",
            "roff": "101",
            "videos": [
              {
                "title": "Deserto del Kalahari",
                "poster": require("./assets/data/poster/deserto-kalahari.png"),
                "file": require("./assets/data/video/deserto-kalahari.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "spreetshoogte-pass",
            "title": "Lo Spreetshoogte Pass",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-23.662923",
            "lng": "16.173612",
            "ron": "5",
            "roff": "6",
            "videos": [
              {
                "title": "Lo Spreetshoogte Pass",
                "poster": require("./assets/data/poster/spreetshoogte-pass.png"),
                "file": require("./assets/data/video/spreetshoogte-pass.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "tropico-del-capricorno",
            "title": "Tropico del Capricorno",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-23.466614",
            "lng": "16.508698",
            "ron": "5",
            "roff": "6",
            "videos": [
              {
                "title": "Tropico del Capricorno",
                "poster": require("./assets/data/poster/tropico-del-capricorno.png"),
                "file": require("./assets/data/video/tropico-del-capricorno.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "kuiseb-pass",
            "title": "Kuiseb Pass",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-23.294496",
            "lng": "15.811479",
            "ron": "3",
            "roff": "4",
            "videos": [
              {
                "title": "Kuiseb Pass",
                "poster": require("./assets/data/poster/kuiseb-pass.png"),
                "file": require("./assets/data/video/kuiseb-pass.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "gaub-pass",
            "title": "Gaub Pass",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "-23.42103",
            "lng": "16.063787",
            "ron": "15",
            "roff": "16",
            "videos": [
              {
                "title": "Gaub Pass",
                "poster": require("./assets/data/poster/gaub-pass.png"),
                "file": require("./assets/data/video/gaub-pass.mp4")
              }
            ],
            "children": []
          }
        ]
      }
    ]
  },
  {
    "slug": "citta",
    "title": "Le Città",
    "subtitle": "Le città della namibia",
    "cover": "",
    "icon": "citta.png",
    "lat": "",
    "lng": "",
    "videos": [],
    "children": [
      {
        "slug": "windhoek",
        "title": "Windhoek la Capitale",
        "subtitle": "",
        "cover": "",
        "icon": "",
        "lat": "-22.574412",
        "lng": "17.079129",
        "ron": "5",
        "roff": "6",
        "videos": [
          {
            "title": "Windhoek la Capitale",
            "poster": require("./assets/data/poster/windhoek.png"),
            "file": require("./assets/data/video/windhoek.mp4")
          }
        ],
        "children": []
      },
      {
        "slug": "swakopmund",
        "title": "Swakopmund",
        "subtitle": "",
        "cover": "",
        "icon": "",
        "lat": "-22.67677",
        "lng": "14.531416",
        "ron": "5",
        "roff": "6",
        "videos": [
          {
            "title": "Swakopmund",
            "poster": require("./assets/data/poster/swakopmund.png"),
            "file": require("./assets/data/video/swakopmund.mp4")
          }
        ],
        "children": []
      },
      {
        "slug": "omaruru",
        "title": "Omaruru",
        "subtitle": "",
        "cover": "",
        "icon": "",
        "lat": "-21.417378",
        "lng": "15.951399",
        "ron": "5",
        "roff": "5.5",
        "videos": [
          {
            "title": "Omaruru",
            "poster": require("./assets/data/poster/omaruru.png"),
            "file": require("./assets/data/video/omaruru.mp4")
          }
        ],
        "children": []
      },
      {
        "slug": "okahandja",
        "title": "Okahandja",
        "subtitle": "",
        "cover": "",
        "icon": "",
        "lat": "-21.979247",
        "lng": "16.912832",
        "ron": "5",
        "roff": "5.5",
        "videos": [
          {
            "title": "Okahandja",
            "poster": require("./assets/data/poster/okahandja.png"),
            "file": require("./assets/data/video/okahandja.mp4")
          }
        ],
        "children": []
      }
    ]
  },
  {
    "slug": "info",
    "title": "Informazioni",
    "subtitle": "",
    "cover": "",
    "icon": "info.png",
    "lat": "",
    "lng": "",
    "videos": [],
    "children": [
      {
        "slug": "popoli",
        "title": "I Popoli",
        "subtitle": "I Popoli della Namibia",
        "cover": "",
        "icon": "nord.png",
        "lat": "",
        "lng": "",
        "videos": [],
        "children": [
          {
            "slug": "himba",
            "title": "Gli Himba",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "",
            "lng": "",
            "videos": [
              {
                "title": "Gli Himba",
                "poster": require("./assets/data/poster/himba.png"),
                "file": require("./assets/data/video/himba.mp4")
              }
            ],
            "children": []
          },
          {
            "slug": "baster",
            "title": "I Baster",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "",
            "lng": "",
            "videos": [
              {
                "title": "I Baster",
                "poster": require("./assets/data/poster/baster.png"),
                "file": require("./assets/data/video/baster.mp4")
              }
            ],
            "children": []
          }
        ]
      },
      {
        "slug": "fauna",
        "title": "Fauna",
        "subtitle": "Fauna della Namibia",
        "cover": "",
        "icon": "",
        "lat": "",
        "lng": "",
        "videos": [],
        "children": [
          {
            "slug": "aminali",
            "title": "Animali ",
            "subtitle": "",
            "cover": "",
            "icon": "",
            "lat": "",
            "lng": "",
            "videos": [
              {
                "title": "Otaria del Capo",
                "poster": require("./assets/data/poster/otaria-del-capo.png"),
                "file": require("./assets/data/video/otaria-del-capo.mp4")
              }
            ],
            "children": []
          }
        ]
      }
    ]
  }
]
